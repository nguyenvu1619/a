const axios = require('axios')
const fs = require('fs-extra')
const GeoJSON = require('geojson')
const path = require('path')
const express = require('express')
const cors = require('cors')
const routes = ["1","2","3","4","5","6","7","8","8A","9","X9","11","12","J14","15","18","19","20","21","22","24","26","28","29","30","31","34","35","36","37","39","43","44","47","48","49","49B","X49","50","51","52","52A","53","53A","54","54A","54B","55","55A","55N","56","57","59","60","62","62H","63","63W","65","66","67","68","70","71","72","73","74","75","76","77","78","79","80","81","81W","82","84","85","85A","86","87","88","90","91","92","93","94","95","96","97","X98","100","103","106","108","111","111A","112","115","119","120","121","124","125","126","134","135","136","143","146","147","148","151","152","155","156","157","165","169","171","172","192","201","206"]

const routesPerQuery = []
let temp = 0
routes.forEach((item, index)=> {
    if(index%10 === 0)   {
        routesPerQuery[index /10] = []
        temp = index /10
    } 
    routesPerQuery[temp].push(item)
})
const routesPerQueryNe = [
    [
      '1', '2', '3',
      '4', '5', '6',
      '7', '8', '8A',
      '9'
    ],
    [
      'X9',  '11', '12',
      'J14', '15', '18',
      '19',  '20', '21',
      '22'
    ],
    [
      '24', '26', '28',
      '29', '30', '31',
      '34', '35', '36',
      '37'
    ],
    [
      '39',  '43',  '44',
      '47',  '48',  '49',
      '49B', 'X49', '50',
      '51'
    ],
    [
      '52',  '52A', '53',
      '53A', '54',  '54A',
      '54B', '55',  '55A',
      '55N'
    ],
    [
      '56', '57',  '59',
      '60', '62',  '62H',
      '63', '63W', '65',
      '66'
    ],
    [
      '67', '68', '70',
      '71', '72', '73',
      '74', '75', '76',
      '77'
    ],
    [
      '78', '79',  '80',
      '81', '81W', '82',
      '84', '85',  '85A',
      '86'
    ],
    [
      '87', '88', '90',
      '91', '92', '93',
      '94', '95', '96',
      '97'
    ],
    [
      'X98',  '100',
      '103',  '106',
      '108',  '111',
      '111A', '112',
      '115',  '119'
    ],
    [
      '120', '121', '124',
      '125', '126', '134',
      '135', '136', '143',
      '146'
    ],
    [
      '147', '148', '151',
      '152', '155', '156',
      '157', '165', '169',
      '171'
    ],
    [ '172', '192', '201', '206' ]
  ]
const app = express()
app.use(cors())

// app.get('/location', async (req,res) => {
//     const data = []
//     await Promise.all(routesPerQueryNe.map(async array => {
//     const result = await axios(`http://www.ctabustracker.com/bustime/api/v2/getvehicles?key=4mV3yityLKdShxUcZD4duyHnZ&rt=${array.join(",")}&tmres=s&format=json`)
//     const tempData = result.data["bustime-response"].vehicle&&result.data["bustime-response"].vehicle.
//     map(item => ({coordinates: [parseFloat(item.lon),parseFloat(item.lat)]}))
//     if(tempData)
//     data.push(...tempData)
//     }))
//     return res.json(data)
// })
app.use(express.static(path.join(__dirname, '../bustracker/build')));
app.get('*', function(req, res) {
  res.sendFile(path.join(__dirname, '../bustracker/build', 'index.html'));
});

const server = require('http').createServer(app).listen(3000)
