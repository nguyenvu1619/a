import {GoogleMapsOverlay as DeckOverlay} from '@deck.gl/google-maps';
import { MVTLayer} from '@deck.gl/geo-layers';

const ICON_MAPPING = {
    marker: { width: 128, height: 128}
  };
const a = ["1","2","3","4","5","6","7","8","8A","9","X9","11","12","J14","15","18","19","20","21","22","24","26","28","29","30","31","34","35","36","37","39","43","44","47","48","49","49B","X49","50","51","52","52A","53","53A","54","54A","54B","55","55A","55N","56","57","59","60","62","62H","63","63W","65","66","67","68","70","71","72","73","74","75","76","77","78","79","80","81","81W","82","84","85","85A","86","87","88","90","91","92","93","94","95","96","97","X98","100","103","106","108","111","111A","112","115","119","120","121","124","125","126","134","135","136","143","146","147","148","151","152","155","156","157","165","169","171","172","192","201","206"]

let overlay
function test(map){
    const test =  new DeckOverlay({
        layers: [
          // new GeoJsonLayer({
          //   id: "earthquakes",
          //   data:  fetch("http://localhost:3000/299.json").then(result => result.json()),
          //   filled: true,
          //   pointRadiusMinPixels: 2,
          //   pointRadiusMaxPixels: 200,
          //   opacity: 0.4,
          //   pointRadiusScale: 0.3,
          //   autoHighlight: true,
          // }),  
          // new GeoJsonLayer({
          //   id: "122",
          //   data:  fetch("http://localhost:3000/okne.json").then(result => result.json()),
          //   filled: true,
          //   pointRadiusMinPixels: 2,
          //   pointRadiusMaxPixels: 200,
          //   opacity: 0.4,
          //   pointRadiusScale: 0.3,
          //   autoHighlight: true,
          // }),  
          // new GeoJsonLayer({
          //   id: "12",
          //   data:  fetch("http://localhost:3000/z/3015.json").then(result => result.json()),
          //   filled: true,
          //   pointRadiusMinPixels: 2,
          //   pointRadiusMaxPixels: 200,
          //   opacity: 0.4,
          //   pointRadiusScale: 0.3,
          //   autoHighlight: true,
          // }),  
          // new GeoJsonLayer({
          //   id: "2",
          //   data:  fetch("http://localhost:3000/z/3057.json").then(result => result.json()),
          //   filled: true,
          //   pointRadiusMinPixels: 2,
          //   pointRadiusMaxPixels: 200,
          //   opacity: 0.4,
          //   pointRadiusScale: 0.3,
          //   autoHighlight: true,
          // }),  
          new MVTLayer({
            getFillColor: [255, 255, 255],
                 opacity: 0.2,
                 pickable: true,
                 maxZoom: 18,
                 minZoom: 1,
                 maxRequests: 4,
                 autoHighlight: true,
                 getLineColor: [80, 80, 80],
                 highlightColor: [239, 219, 137, 85],
                 getLineWidth: 0.4,
                 lineWidthScale: 2,
                 lineWidthUnits:'pixels',
                 data:"http://54.158.38.22:3000/tile" + '/{z}/{x}/{y}.pbf',
          })
]
});
if(overlay) overlay.setMap(null)
overlay = test
      overlay.setMap(map);
}

export default test